from flask import Flask, render_template, make_response
import os


app = Flask(__name__)

@app.route("/<path:name>")
def hello(name):
    
    p = 'templates/' + name
    file_type = name.split('.')[-1]

    if (file_type != 'html') and (file_type != 'css') and (file_type != 'ico'):
        return error_403(403)
    elif ('//' in name) or ('~' in name) or ('..' in name):
        return error_403(403)
    elif not os.path.isfile(p):
        return error_404(404)


    return render_template(name)

@app.errorhandler(404)
def error_404(error):
    return render_template("404.html"), 404
    

@app.errorhandler(403)
def error_403(error):
    return render_template('403.html'), 403

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)
